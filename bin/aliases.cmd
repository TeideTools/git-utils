@ECHO OFF
REM This file is not ment to be edited by the user.
REM instead edit aliases_user.cmd

rem echo [PIPE] aliases loaded!

setlocal
rem Get the current directory
set "currentDir=%~dp0"
cd /d "%currentDir%\.."
set "parentDir=%CD%"

REM [STUDIO] Aliases
doskey editaliases=%parentDir%/src/createOrOpenFile.cmd %currentDir%/aliases.cmd
doskey editaliases-user=%parentDir%/src/createOrOpenFile.cmd %currentDir%/aliases_user.cmd
doskey pycharm="C:/Program Files/JetBrains/PyCharm Community Edition 2023.2.4/bin/pycharm64.exe" $*

REM General OS
doskey ls=dir
doskey home=cd %USERPROFILE%
doskey repos=cd %USERPROFILE%/Desktop/_Repos
doskey pipe=cd c:/works/git
doskey pipe-config=cd %USERPROFILE%/.teide/
doskey dev-pipe=cd %USERPROFILE%/Desktop/_Repos/bmLucasMorante/pipe/

REM [GIT] Commands
set "MASTER_BRANCH=main"
set "DEV_BRANCH=dev"

REM dev-log
REM >> Show the commits of a feature branch excluding the dev branch commits.
doskey dev-log=^
git log -8 --graph --oneline --decorate ^&^ echo Max count limit reached...

REM dev-feature-log
REM >> Show the commits of a feature branch excluding the dev branch commits.
doskey dev-feature-log=^
git log %DEV_BRANCH%..$* -32 --graph --oneline --decorate ^&^ echo Max count limit reached...

REM dev-setup
REM >> Configure a DEV branch in case the repo doesnt have one.
doskey dev-setup=^
git checkout -b %DEV_BRANCH% ^&^

REM dev-update
REM >> Updates the dev branch to be on top of the latest changes in main.
doskey dev-update=^
git checkout %MASTER_BRANCH% ^&^
git pull ^&^
git rebase %MASTER_BRANCH% %DEV_BRANCH%

REM dev-feature-create
REM >> Creates a new feature branch under the dev branch.
doskey dev-feature-create=^
git checkout %MASTER_BRANCH% ^&^
git pull ^&^
git rebase %MASTER_BRANCH% %DEV_BRANCH% ^&^
git checkout %DEV_BRANCH% ^&^
git checkout -b $* ^&^
git rebase %DEV_BRANCH% $* ^&^
git checkout $*

REM dev-feature-update
REM >> Updates the feature branch to be on top of the latest changes in main and dev.
doskey dev-feature-update=^
git checkout %MASTER_BRANCH% ^&^
git pull ^&^
git rebase %MASTER_BRANCH% %DEV_BRANCH% ^&^
git checkout $* ^&^
git rebase %DEV_BRANCH% $* ^&^
git checkout -b $*

REM dev-feature-release
REM >> Replays the feature commits on top of the main branch excluding the dev branch commits.
rem git log dev..$* --oneline ^&^
doskey dev-feature-release=^
git checkout %MASTER_BRANCH% ^&^
for /F "tokens=1" %%i in ('git log %DEV_BRANCH%..$* --oneline --reverse') do git cherry-pick %%i


REM [Python & venvs]
rem todo: add pip list after activating
doskey activate=".venv/Scripts/activate" ^& pip list
doskey pipe-activate=".venv_py39_windows/Scripts/activate" ^& pip list


REM [LINUX] mimic Bash-like behavior in Windows Command Prompt
REM List files and directories in bare format
doskey ls=dir /b $*

REM List files and directories in detailed (long) format, ordered by name
doskey ll=dir /o-n /b $*

REM Concatenate and display the contents of a file
doskey cat=type $*

REM Clear the Command Prompt screen
doskey clear=cls

REM Create a new file or update the timestamp of an existing file
doskey touch=copy con $*

REM Delete files
rem doskey rm=del

REM Move or rename files and directories
rem doskey mv=move
REM Usage: mv <source> <destination>

REM Copy files and directories
rem doskey cp=copy
REM Usage: cp <source> <destination>

REM Create a new directory
rem doskey mkdir=md
REM Usage: mkdir <directory_name>

REM Remove a directory and its contents (recursively and quietly)
rem doskey rmdir=rmdir /s /q
REM Usage: rmdir <directory_name>

REM Search for patterns in files
doskey grep=findstr
REM Usage: grep <pattern> <file_path>

REM Open a text editor (Notepad) for editing text files
doskey nano=notepad $*
REM Usage: nano <file_path>
