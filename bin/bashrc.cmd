@ECHO OFF
REM This file is not ment to be edited by the user.
REM instead edit bashrc_user.cmd

rem echo [PIPE] bashrc loaded!

setlocal
rem Get the current directory
set "currentDir=%~dp0"
cd /d "%currentDir%\.."
set "parentDir=%CD%"

REM [STUDIO] Environment
REM Global studio terminal commands go here:


rem end of Global studio terminal commands

REM [STUDIO] Aliases
if exist "%currentDir%\aliases.cmd" (
    call "%currentDir%\aliases.cmd"
)

REM [USER] Config bashrc_user.cmd file
if exist "%currentDir%\bashrc_user.cmd" (
    call "%currentDir%\bashrc_user.cmd"
)

REM [USER] Aliases
if exist "%currentDir%\aliases_user.cmd" (
    call "%currentDir%\aliases_user.cmd"
)