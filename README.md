# git-utils

### Getting Started

- clone
- venv
- requirements
- [optional] copy bashrc and aliases to custom directories
- [optional] copy src scripts to custom directories
- add registry key to bootstrap to all terminals
- uninstall (registry, custom directories )

The setup workflow looks like this:
First the repo is cloned anywhere. Then the setup.bat will be run, 
This will crete the venv and install the requirements.



Then the installation command should be run. It will add 

### TODO
- install script
  - read config roots
  - copy files to roots if necesary
  - add registry key
- develop git scripts and other utils
  - editaliases -- createOrOpenFile
  - listAliases -- 
  - git-utils -- list git utils
  - dev-setup -- start_dev_config_branch(source_branch="main")
    - git checkout 'main'
    - git pull
    - git -b 'dev'
  - dev-update -- dev-config-update.cmd
    - git checkout 'main'
    - gir rebase 'dev' 'main'
    - git checkout 'dev'
    - git pull 'origin/dev'
  - feature-create -- dev-new-feature.cmd
    - dev-setup
    - dev-update
    - git checkout 'dev'
    - git checkout -b dev.feature
  - feature-release -- dev-feature-release.cmd
    - git checkout dev.feature
    - [optional] git squash
    - git checkout main
    - collect dev.feature commits
    - git cherry-pick collected_commits


The idea is to have a repo that will add shell commands to the terminal.
There will be a config file that will set the installation paths for the global and user paths
a install script with arguments for the config file and a uninstall flag.
note: for now only windows support

we want to have a global shell commands hook similar to bashrc in Linux and another for user-level configuration.

There will be "header" file that is the main hook to the terminal environment. 
It will call the global bashrc file and the user bashrc_local file.\
Each of these bashrc files, make a call to a relative .aliases file where the aliases are added.
Is separated in different files to have a more organized and simpler to read bashrc files.

Folder Structure:
- .venv
- bin
  - install <settings.cfg> --uninstall
  - aliases.cmd
  - aliases_user.cmd
  - bashrc.cmd
  - bashrc_user.cmd
- src
  - git-utils.py
  - dev-config-update.cmd
  - dev-new-feature.cmd
  - dev-feature-release.cmd
- install.bat <settings.cfg> --uninstall
  - install repo: create venv + add pip requirements
  - use script arguments and use settings config to install the bashrc & alias files
- requirements.txt
- README.md