@echo off
REM This file contains the createOrOpenFile function logic

:: Check if an argument (path to aliases.cmd) is provided
if "%~1"=="" (
    echo Path to aliases.cmd not provided. Exiting.
    exit /b
)

:createOrOpenAliasesFile
if exist "%~1" (
    echo Aliases file found. Calling it...
    notepad "%~1"
) else (
    echo Aliases file not found. Creating it...
    echo @echo off > "%~1"
    echo REM This file is meant for user editing. >> "%~1"
    echo REM Please make user-specific changes here. >> "%~1"
    notepad "%~1"
)
exit /b
