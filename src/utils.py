import os
import sys
import json
import subprocess
import tempfile
import configparser
import winreg
from typing import Optional

STUDIO_BASHRC = 'bashrc.cmd'
STUDIO_ALIASES = 'aliases.cmd'
USER_BASHRC = 'bashrc_user.cmd'
USER_ALIASES = 'aliases_user.cmd'

script_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.dirname(script_dir)


def writeToJSONFile(path, data, fileName=None):
    if fileName:
        path = path + '/' + fileName + '.json'
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'w') as fp:
        json.dump(data, fp, indent=4, sort_keys=True)
        return path


def readJSONFile(path, fileName=None):
    if fileName:
        path = path + '/' + fileName + '.json'
    try:
        with open(path) as json_file:
            json_data = json.load(json_file)
        return json_data
    except json.JSONDecodeError as e:
        raise ValueError(f"Invalid JSON: {e}")


def cmd_exec(cmd, show_shell=False):
    log = ''
    if show_shell:
        with tempfile.TemporaryFile() as f:
            proc = subprocess.Popen(cmd, shell=not show_shell, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            for c in iter(lambda: proc.stdout.read(1), b''):  # replace '' with b'' for Python 3
                sys.stdout.write(c)
                log += c
                f.write(c)

        retval = proc.wait()
        f.close()
    else:
        proc = subprocess.Popen(cmd, shell=not show_shell, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in proc.stdout.readlines():
            log += str(line)
        retval = proc.wait()
        # print(log)

    return retval, log


def get_settings_root(location: str, config_file: Optional[str] = None):
    if config_file is None:
        config_file = os.path.join(parent_dir, "settings.json")
    config = readJSONFile(config_file)
    location = config['root'][location]
    if location is None:
        location = os.path.join(parent_dir, 'bin')
    return os.path.expandvars(location)


def get_config_value(config_file, section, key):
    script_dir = os.path.dirname(os.path.abspath(__file__))
    # parent_dir = os.path.abspath(os.path.join(script_dir, os.pardir))
    config_path = os.path.join(parent_dir, config_file)

    if not os.path.exists(config_path):
        return None

    config = configparser.ConfigParser()
    config.read(config_path)

    if section in config and key in config[section]:
        return config[section][key]
    else:
        return None


def copy_files(config_file):
    if not os.path.exists(config_file):
        print("Config file does not exist.")
        return

    config = configparser.ConfigParser()
    config.read(config_file)

    print(parent_dir)
    # source_bashrc_path = os.path.join(parent_dir, "/bin/bashrc.cmd")
    source_bashrc_path = parent_dir + "/bin/bashrc.cmd"
    print(source_bashrc_path)
    destination = get_config_value(config_file, 'user', 'aliases')
    # print(destination)


def add_registry_key(key_path, key_type, key_value):
    try:
        # Map the string type to winreg type constants
        value_types = {
            "REG_SZ": winreg.REG_SZ,
            "REG_EXPAND_SZ": winreg.REG_EXPAND_SZ,
            "REG_DWORD": winreg.REG_DWORD,
            "REG_QWORD": winreg.REG_QWORD,
            "REG_BINARY": winreg.REG_BINARY,
            "REG_MULTI_SZ": winreg.REG_MULTI_SZ,
        }

        if key_type not in value_types:
            raise ValueError("Invalid value type specified.")

        # Split the key path into base key, subkey, and value name
        parts = key_path.split("\\")
        base_key_str = parts[0]
        sub_key = "\\".join(parts[1:-1])
        value_name = parts[-1]

        # Map the base key string to the corresponding winreg constant
        base_keys = {
            "HKEY_LOCAL_MACHINE": winreg.HKEY_LOCAL_MACHINE,
            "HKEY_CURRENT_USER": winreg.HKEY_CURRENT_USER,
            "HKEY_CLASSES_ROOT": winreg.HKEY_CLASSES_ROOT,
            "HKEY_USERS": winreg.HKEY_USERS,
            "HKEY_CURRENT_CONFIG": winreg.HKEY_CURRENT_CONFIG,
        }

        if base_key_str not in base_keys:
            raise ValueError("Invalid base key specified.")

        base_key = base_keys[base_key_str]

        # Open or create the key where you want to add the new value
        key = winreg.CreateKey(base_key, sub_key)

        # Set the value in the subkey
        winreg.SetValueEx(key, value_name, 0, value_types[key_type], key_value)

        # Close the key
        winreg.CloseKey(key)

        print(f"Registry key added successfully. {key_path}")
    except PermissionError:
        raise ValueError("Permission denied. Please run the script as an administrator.")
    except Exception as e:
        raise ValueError(f"An error occurred: {e}")


def main():
    # key_path = r"HKEY_CURRENT_USER\SOFTWARE\Microsoft\Command Processor\AutoRun"
    key_path = r"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Command Processor\AutoRun"
    key_type = "REG_EXPAND_SZ"

    studio_root = get_settings_root(location="studio")
    local_root = get_settings_root(location="local")
    studio_bashrc_path = os.path.join(studio_root, STUDIO_BASHRC)

    add_registry_key(key_path, key_type, studio_bashrc_path)


if __name__ == "__main__":
    """
    bashrc.cmd looks for a relative aliases.cmd file to run. Same for the local/user version.
    
    If the config is !null then we will copy some files to those directories during the installation process.
    And removing them during the uninstallation. 
    
    These files include the bin bashrc, aliases etc and the src/* folder.
    
    """
    main()

    exit()

    # If roots are set, we need to use those paths to copy the bashrc and aliases to that destination.
    studio_root = get_settings_root(location="studio")
    local_root = get_settings_root(location="local")

    print(studio_root)
    print(local_root)

    # def copy_files_to_location():
    #     """Will copy the required files to its location based in the config file"""
    #     # Read config
    #     # If not None; collect files and src; copy to location

    # Add registry key to bootstrap the terminal at startup
    studio_bashrc_path = os.path.join(studio_root, STUDIO_BASHRC)


    command = 'reg add "HKCU\\SOFTWARE\\Microsoft\\Command Processor" /v AutoRun /t REG_EXPAND_SZ /d "{}" /f'.format(studio_bashrc_path)
    cmd_exec(command)
    print("Registry key added: %s" % command)


