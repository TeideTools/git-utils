@echo off

REM Setup process of the repo
python -m venv .venv
call .venv/Scripts/activate && (
    python -m pip install --upgrade pip
    pip install -r ./requirements.txt
    python src/utils.py
    pause
)